<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cars".
 *
 * @property int $car_id
 * @property string|null $car_name
 * @property string|null $car_band
 * @property string|null $car_no
 * @property int|null $car_year
 * @property string|null $car_maintenance
 * @property string|null $car_owner
 */
class Cars extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cars';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['car_year'], 'integer'],
            [['car_maintenance','car_check_date'], 'safe'],
            [['car_name', 'car_band', 'car_no', 'car_owner'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'car_id' => 'Car ID',
            'car_name' => 'ชื่อรถ',
            'car_band' => 'ยี่ห้อรถ',
            'car_no' => 'ป้ายทะเบียนรถ',
            'car_year' => 'รถปี',
            'car_maintenance' => 'วันที่ซ่อมบำรุงประจำปี',
            'car_check_date' => 'วันที่ตรวจสภาพรถล่าสุด',
            'car_owner' => 'ผู้รับผิดชอบรถ',
        ];
    }
}
