<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Cars;

/**
 * CarsSearch represents the model behind the search form of `app\models\Cars`.
 */
class CarsSearch extends Cars
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['car_id', 'car_year'], 'integer'],
            [['car_name', 'car_band', 'car_no', 'car_maintenance','car_check_date', 'car_owner'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Cars::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'car_id' => $this->car_id,
            'car_year' => $this->car_year,
            'car_maintenance' => $this->car_maintenance,
            'car_check_date' => $this->car_check_date,
        ]);

        $query->andFilterWhere(['like', 'car_name', $this->car_name])
            ->andFilterWhere(['like', 'car_band', $this->car_band])
            ->andFilterWhere(['like', 'car_no', $this->car_no])
            ->andFilterWhere(['like', 'car_owner', $this->car_owner]);

        return $dataProvider;
    }
}
