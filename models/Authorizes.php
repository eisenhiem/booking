<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "authorizes".
 *
 * @property int $authorize_id
 * @property string|null $authorize_name
 * @property string|null $authorize_position_1
 * @property string|null $authorize_position_2
 * @property string|null $authorize_position_3
 * @property string|null $d_update
 */
class Authorizes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'authorizes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['d_update'], 'safe'],
            [['authorize_name', 'authorize_position_1', 'authorize_position_2', 'authorize_position_3'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'authorize_id' => 'Authorize ID',
            'authorize_name' => 'ชื่อผู้มีอำนาจอนุมัติ',
            'authorize_position_1' => 'ตำแหน่ง 1',
            'authorize_position_2' => 'ตำแหน่ง 2',
            'authorize_position_3' => 'ตำแหน่ง 3',
            'd_update' => 'D Update',
        ];
    }
}
