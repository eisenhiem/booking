<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "rooms".
 *
 * @property int $room_id
 * @property string|null $room_name
 * @property string|null $room_color
 * @property int|null $room_capacity
 */
class Rooms extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'rooms';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['room_capacity'], 'integer'],
            [['room_name'], 'string', 'max' => 100],
            [['room_color'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'room_id' => 'รหัสห้องประชุม',
            'room_name' => 'ห้องประชุม',
            'room_color' => 'สีของห้องประชุม',
            'room_capacity' => 'ความจุของห้องประชุม',
        ];
    }
}
