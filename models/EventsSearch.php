<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Events;

/**
 * EventsSearch represents the model behind the search form of `app\models\Events`.
 */
class EventsSearch extends Events
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['event_id', 'room_id', 'participants'], 'integer'],
            [['event_name', 'start_date', 'end_date', 'snacks', 'lunch', 'projector', 'conference', 'recorder', 'request_by', 'request_date','eventColor'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Events::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'event_id' => $this->event_id,
            'room_id' => $this->room_id,
            'start_date' => $this->start_date,
            'end_date' => $this->end_date,
            'participants' => $this->participants,
            'request_date' => $this->request_date,
        ]);

        $query->andFilterWhere(['like', 'event_name', $this->event_name])
            ->andFilterWhere(['like', 'snacks', $this->snacks])
            ->andFilterWhere(['like', 'lunch', $this->lunch])
            ->andFilterWhere(['like', 'projector', $this->projector])
            ->andFilterWhere(['like', 'conference', $this->conference])
            ->andFilterWhere(['like', 'recorder', $this->recorder])
            ->andFilterWhere(['like', 'request_by', $this->request_by])
            ->andFilterWhere(['like', 'eventColor', $this->eventColor]);

        return $dataProvider;
    }
}
