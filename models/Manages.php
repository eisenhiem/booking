<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "manages".
 *
 * @property int $manage_id
 * @property string|null $manage_name
 * @property string|null $manage_position
 * @property string|null $d_update
 */
class Manages extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'manages';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['d_update'], 'safe'],
            [['manage_name', 'manage_position'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'manage_id' => 'Manage ID',
            'manage_name' => 'ชื่อผู้จัดรถ',
            'manage_position' => 'ตำแหน่งผู้จัดรถ',
            'd_update' => 'D Update',
        ];
    }
}
