<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "checks".
 *
 * @property int $check_id
 * @property int|null $req_id
 * @property int|null $car_id
 * @property string|null $chk_date
 * @property string|null $chk_fuel
 * @property string|null $fuel_comment
 * @property string|null $chk_wheel
 * @property string|null $wheel_comment
 * @property string|null $chk_outside
 * @property string|null $outside_comment
 * @property string|null $chk_mile
 * @property string|null $mile_comment
 * @property string|null $chk_status
 * @property string|null $sender
 * @property string|null $reciever
 */
class Checks extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'checks';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['req_id', 'car_id'], 'integer'],
            [['chk_date'], 'safe'],
            [['chk_fuel', 'chk_wheel', 'chk_outside', 'chk_mile', 'chk_status'], 'string'],
            [['fuel_comment', 'wheel_comment', 'outside_comment', 'mile_comment', 'sender', 'reciever'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'check_id' => 'Check ID',
            'req_id' => 'Req ID',
            'car_id' => 'Car ID',
            'chk_date' => 'วันที่ตรวจ',
            'chk_fuel' => 'น้ำมัน',
            'fuel_comment' => 'จำนวน น้ำมัน',
            'chk_wheel' => 'ล้อ ลมยาง',
            'wheel_comment' => 'สภาพ ล้อ ลมยาง',
            'chk_outside' => 'ภายนอกตัวรถ',
            'outside_comment' => 'สภาพภายนอกตัวรถ',
            'chk_mile' => 'เข็มไมล์',
            'mile_comment' => 'เลขไมล์ (กิโลเมตร)',
            'chk_status' => 'ประเภทการตรวจ',
            'sender' => 'ผู้ส่ง',
            'reciever' => 'ผู้รับ',
        ];
    }
}
