<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "area".
 *
 * @property int $area_id
 * @property int|null $province_id
 * @property string|null $area_name
 */
class Area extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'area';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['province_id'], 'integer'],
            [['area_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'area_id' => 'Area ID',
            'province_id' => 'Province ID',
            'area_name' => 'พื้นที่เสี่ยง',
        ];
    }
}
