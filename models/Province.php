<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "province".
 *
 * @property int $province_id
 * @property string|null $province_name
 */
class Province extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'province';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['province_id'], 'required'],
            [['province_id'], 'integer'],
            [['province_name'], 'string', 'max' => 255],
            [['province_id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'province_id' => 'Province ID',
            'province_name' => 'จังหวัด',
        ];
    }
}
