<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Reqs;

/**
 * ReqsSearch represents the model behind the search form of `app\models\Reqs`.
 */
class ReqsSearch extends Reqs
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['req_id', 'passengers', 'authorize_id', 'car_id', 'driver_id', 'manage_id'], 'integer'],
            [['req_date', 'req_by', 'req_position', 'req_location', 'req_cause', 'begin_datetime', 'end_datetime', 'authorize_date'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Reqs::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'req_id' => $this->req_id,
            'req_date' => $this->req_date,
            'passengers' => $this->passengers,
            'begin_datetime' => $this->begin_datetime,
            'end_datetime' => $this->end_datetime,
            'authorize_id' => $this->authorize_id,
            'authorize_date' => $this->authorize_date,
            'car_id' => $this->car_id,
            'driver_id' => $this->driver_id,
            'manage_id' => $this->manage_id,
        ]);

        $query->andFilterWhere(['like', 'req_by', $this->req_by])
            ->andFilterWhere(['like', 'req_position', $this->req_position])
            ->andFilterWhere(['like', 'req_location', $this->req_location])
            ->andFilterWhere(['like', 'req_cause', $this->req_cause]);

        return $dataProvider;
    }
}
