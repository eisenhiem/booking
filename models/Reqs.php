<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "reqs".
 *
 * @property int $req_id
 * @property string|null $req_date
 * @property string|null $req_by
 * @property string|null $req_position
 * @property string|null $req_location
 * @property string|null $req_cause
 * @property int|null $passengers
 * @property string|null $begin_datetime
 * @property string|null $end_datetime
 * @property int|null $authorize_id
 * @property string|null $authorize_date
 * @property int|null $car_id
 * @property int|null $driver_id
 * @property int|null $manage_id
 */
class Reqs extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'reqs';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['req_date', 'begin_datetime', 'end_datetime', 'authorize_date'], 'safe'],
            [['passengers', 'authorize_id', 'car_id', 'driver_id', 'manage_id'], 'integer'],
            [['req_by', 'req_position'], 'string', 'max' => 100],
            [['req_location', 'req_cause'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'req_id' => 'Req ID',
            'req_date' => 'วันที่ขอรถ',
            'req_by' => 'ผู้ขอใช้รถ',
            'req_position' => 'ตำแหน่งผู้ขอใช้รถ',
            'req_location' => 'ขอใช้รถไปที่',
            'req_cause' => 'เหตุผลการขอใช้รถ',
            'passengers' => 'จำนวนผู้โดยสาร',
            'begin_datetime' => 'วันเวลาที่เริ่มใช้',
            'end_datetime' => 'วันเวลาสิ้นสุดการใช้',
            'authorize_id' => 'ผู้อนุญาตให้ใช้รถ',
            'authorize_date' => 'วันที่อนุมัติ',
            'car_id' => 'ยานพาหนะ',
            'driver_id' => 'พนักงานขับรถ',
            'manage_id' => 'ผู้จัดการใช้รถ',
        ];
    }
}
