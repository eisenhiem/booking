<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "events".
 *
 * @property int $event_id
 * @property int|null $room_id
 * @property string|null $event_name
 * @property string|null $start_date
 * @property string|null $end_date
 * @property int|null $participants
 * @property string|null $snacks
 * @property string|null $projector
 * @property string|null $conference
 * @property string|null $recorder
 * @property string|null $request_by
 * @property string|null $request_date
 */
class Events extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'events';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['room_id', 'participants'], 'integer'],
            [['start_date', 'end_date', 'request_date'], 'safe'],
            [['snacks', 'projector', 'conference', 'recorder','lunch'], 'string'],
            [['event_name', 'request_by','eventColor'], 'string', 'max' => 255],
            [['start_date', 'end_date','event_name', 'request_by','room_id', 'participants',], 'required'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'event_id' => 'ลำดับที่',
            'room_id' => 'ห้อง',
            'event_name' => 'เรื่อง',
            'start_date' => 'วันเวลาที่เริ่มการประชุม',
            'end_date' => 'วันเวลาสิ้นสุดการประชุม',
            'participants' => 'จำนวนผู้เข้าร่วม',
            'snacks' => 'อาหารว่าง',
            'lunch' => 'อาหารเที่ยง',
            'projector' => 'โปรเจ็คเตอร์',
            'conference' => 'ประชุมทางไกล',
            'recorder' => 'เครื่องอัดเสียง',
            'request_by' => 'ผู้ขอใช้ห้องประชุม',
            'request_date' => 'วันที่ขอใช้',
            'eventColor' => 'สี',
        ];
    }

    public function getRooms(){
        return $this->hasOne(Rooms::className(), ['room_id' => 'room_id']);
    }
}
