<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Checks;

/**
 * ChecksSearch represents the model behind the search form of `app\models\Checks`.
 */
class ChecksSearch extends Checks
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['check_id', 'req_id', 'car_id'], 'integer'],
            [['chk_date', 'chk_fuel', 'fuel_comment', 'chk_wheel', 'wheel_comment', 'chk_outside', 'outside_comment', 'chk_mile', 'mile_comment', 'chk_status', 'sender', 'reciever'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Checks::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'check_id' => $this->check_id,
            'req_id' => $this->req_id,
            'car_id' => $this->car_id,
            'chk_date' => $this->chk_date,
        ]);

        $query->andFilterWhere(['like', 'chk_fuel', $this->chk_fuel])
            ->andFilterWhere(['like', 'fuel_comment', $this->fuel_comment])
            ->andFilterWhere(['like', 'chk_wheel', $this->chk_wheel])
            ->andFilterWhere(['like', 'wheel_comment', $this->wheel_comment])
            ->andFilterWhere(['like', 'chk_outside', $this->chk_outside])
            ->andFilterWhere(['like', 'outside_comment', $this->outside_comment])
            ->andFilterWhere(['like', 'chk_mile', $this->chk_mile])
            ->andFilterWhere(['like', 'mile_comment', $this->mile_comment])
            ->andFilterWhere(['like', 'chk_status', $this->chk_status])
            ->andFilterWhere(['like', 'sender', $this->sender])
            ->andFilterWhere(['like', 'reciever', $this->reciever]);

        return $dataProvider;
    }
}
