<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Authorizes;

/**
 * AuthorizesSearch represents the model behind the search form of `app\models\Authorizes`.
 */
class AuthorizesSearch extends Authorizes
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['authorize_id'], 'integer'],
            [['authorize_name', 'authorize_position_1', 'authorize_position_2', 'authorize_position_3', 'd_update'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Authorizes::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'authorize_id' => $this->authorize_id,
            'd_update' => $this->d_update,
        ]);

        $query->andFilterWhere(['like', 'authorize_name', $this->authorize_name])
            ->andFilterWhere(['like', 'authorize_position_1', $this->authorize_position_1])
            ->andFilterWhere(['like', 'authorize_position_2', $this->authorize_position_2])
            ->andFilterWhere(['like', 'authorize_position_3', $this->authorize_position_3]);

        return $dataProvider;
    }
}
