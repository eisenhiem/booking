<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "drivers".
 *
 * @property int $driver_id
 * @property string|null $driver_name
 * @property string|null $driver_license
 * @property string|null $license_expire
 * @property string|null $driver_contact
 * @property string|null $d_update
 */
class Drivers extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'drivers';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['license_expire', 'd_update'], 'safe'],
            [['driver_name', 'driver_license', 'driver_contact'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'driver_id' => 'Driver ID',
            'driver_name' => 'ชื่อผู้ขับ',
            'driver_license' => 'เลขที่ใบอนุญาตขับขี่',
            'license_expire' => 'วันที่หมดอายุใบอนุญาต',
            'driver_contact' => 'เบอร์โทรติดต่อ',
            'd_update' => 'D Update',
        ];
    }
}
