/*
Navicat MySQL Data Transfer

Source Server         : Local_DB
Source Server Version : 50565
Source Host           : localhost:3306
Source Database       : booking

Target Server Type    : MYSQL
Target Server Version : 50565
File Encoding         : 65001

Date: 2020-09-18 11:31:19
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `authorizes`
-- ----------------------------
DROP TABLE IF EXISTS `authorizes`;
CREATE TABLE `authorizes` (
`authorize_id`  int(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
`authorize_name`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
`authorize_position_1`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`authorize_position_2`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`authorize_position_3`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`d_update`  timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP ,
PRIMARY KEY (`authorize_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=3

;

-- ----------------------------
-- Records of authorizes
-- ----------------------------
BEGIN;
INSERT INTO `authorizes` VALUES ('1', 'นางสาวธรรมพร ปรัสพันธ์', 'ผู้อำนวยการโรงพยาบาลเหล่าเสือโก้ก', '', '', '2020-05-12 11:12:28'), ('2', 'นางพัชรา เดชาวัตร', 'นักจัดการงานทั่วไปชำนาญการ', 'ปฏิบัติราชการแทนผู้อำนวยการโรงพยาบาลเหล่าเสือโก้ก', '', '2020-05-12 11:12:08');
COMMIT;

-- ----------------------------
-- Table structure for `cars`
-- ----------------------------
DROP TABLE IF EXISTS `cars`;
CREATE TABLE `cars` (
`car_id`  int(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
`car_name`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`car_band`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`car_no`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`car_year`  int(11) NULL DEFAULT NULL ,
`car_check_date`  date NULL DEFAULT NULL ,
`car_maintenance`  date NULL DEFAULT NULL ,
`car_owner`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
PRIMARY KEY (`car_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=1

;

-- ----------------------------
-- Records of cars
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for `checks`
-- ----------------------------
DROP TABLE IF EXISTS `checks`;
CREATE TABLE `checks` (
`check_id`  int(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
`req_id`  int(11) NULL DEFAULT NULL ,
`car_id`  int(11) NULL DEFAULT NULL ,
`chk_date`  datetime NULL DEFAULT NULL ,
`chk_fuel`  enum('1','0') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`fuel_comment`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`chk_wheel`  enum('1','0') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`wheel_comment`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`chk_outside`  enum('1','0') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`outside_comment`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`chk_mile`  enum('1','0') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`mile_comment`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`chk_status`  enum('2','1') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`sender`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`reciever`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
PRIMARY KEY (`check_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=1

;

-- ----------------------------
-- Records of checks
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for `drivers`
-- ----------------------------
DROP TABLE IF EXISTS `drivers`;
CREATE TABLE `drivers` (
`driver_id`  int(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
`driver_name`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`driver_license`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`license_expire`  date NULL DEFAULT NULL ,
`driver_contact`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`d_update`  timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP ,
PRIMARY KEY (`driver_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=2

;

-- ----------------------------
-- Records of drivers
-- ----------------------------
BEGIN;
INSERT INTO `drivers` VALUES ('1', 'นายสิทธิเดช ละดาห์', '62000705', '2024-06-05', '0921298657', '2020-09-18 10:35:06');
COMMIT;

-- ----------------------------
-- Table structure for `events`
-- ----------------------------
DROP TABLE IF EXISTS `events`;
CREATE TABLE `events` (
`event_id`  int(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
`room_id`  int(10) UNSIGNED NULL DEFAULT NULL ,
`event_name`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`start_date`  datetime NULL DEFAULT NULL ,
`end_date`  datetime NULL DEFAULT NULL ,
`participants`  int(11) NULL DEFAULT NULL ,
`snacks`  enum('1','0') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' ,
`lunch`  enum('1','0') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' ,
`projector`  enum('1','0') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' ,
`conference`  enum('1','0') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' ,
`recorder`  enum('1','0') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' ,
`request_by`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`request_date`  timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP ,
`eventColor`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
PRIMARY KEY (`event_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=5

;

-- ----------------------------
-- Records of events
-- ----------------------------
BEGIN;
INSERT INTO `events` VALUES ('1', '1', 'EOC', '2020-04-29 08:00:00', '2020-04-29 12:00:00', '10', '1', '0', '1', '1', '0', 'นายจักรพงษ์ วงศ์กมลาไสย', '2020-04-29 10:43:07', 'Red'), ('2', '1', 'EOC', '2020-05-04 08:00:04', '2020-05-04 12:00:04', '10', '1', '0', '1', '1', '0', 'นายจักรพงษ์ วงศ์กมลาไสย', '2020-04-29 10:43:11', 'Blue'), ('3', '1', 'EOC', '2020-05-06 08:00:39', '2020-05-06 12:00:39', '10', '1', '0', '1', '1', '0', 'นายจักรพงษ์ วงศ์กมลาไสย', '2020-04-29 10:43:17', 'Green'), ('4', '1', 'EOC', '2020-05-11 08:00:53', '2020-05-11 12:00:53', '8', '1', '0', '0', '1', '0', 'นายจักรพงษ์ วงศ์กมลาไสย', '2020-04-29 11:52:01', 'Green');
COMMIT;

-- ----------------------------
-- Table structure for `manages`
-- ----------------------------
DROP TABLE IF EXISTS `manages`;
CREATE TABLE `manages` (
`manage_id`  int(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
`manage_name`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`manage_position`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`d_update`  timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP ,
PRIMARY KEY (`manage_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=1

;

-- ----------------------------
-- Records of manages
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for `reqs`
-- ----------------------------
DROP TABLE IF EXISTS `reqs`;
CREATE TABLE `reqs` (
`req_id`  int(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
`req_date`  date NULL DEFAULT NULL ,
`req_by`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`req_position`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`req_location`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`req_cause`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`passengers`  int(11) NULL DEFAULT NULL ,
`begin_datetime`  datetime NULL DEFAULT NULL ,
`end_datetime`  datetime NULL DEFAULT NULL ,
`authorize_id`  int(11) NULL DEFAULT NULL ,
`authorize_date`  date NULL DEFAULT NULL ,
`car_id`  int(11) NULL DEFAULT NULL ,
`driver_id`  int(11) NULL DEFAULT NULL ,
`manage_id`  int(11) NULL DEFAULT NULL ,
PRIMARY KEY (`req_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=4

;

-- ----------------------------
-- Records of reqs
-- ----------------------------
BEGIN;
INSERT INTO `reqs` VALUES ('1', '2020-05-12', 'นายจักรพงษ์ วงศ์กมลาไสย', 'นักวิชาการคอมพิวเตอร์', 'สสจ.อุบล', 'ไปประชุมงาน EOC', '1', '2020-05-13 08:30:59', '2020-05-13 16:30:59', null, null, null, null, null), ('2', '2020-05-20', 'นาย สถาพร แช่มชื่น', 'พนักงานบริการ', 'สสจ.อุบล', 'ออกหน่วย', '1', '2020-05-20 12:56:21', '2020-05-20 16:50:53', null, null, null, null, null), ('3', '2020-05-20', 'จินตนา บุญสุข', 'นักวิชาการเงินและบัญชี', 'ร้านวิริยะเภสัช', 'รับครุภัณฑ์การแพทย์', '0', '2020-05-20 08:55:49', '2020-05-20 12:55:49', null, null, null, null, null);
COMMIT;

-- ----------------------------
-- Table structure for `rooms`
-- ----------------------------
DROP TABLE IF EXISTS `rooms`;
CREATE TABLE `rooms` (
`room_id`  int(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
`room_name`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`room_color`  varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`room_capacity`  int(5) NULL DEFAULT NULL ,
PRIMARY KEY (`room_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=2

;

-- ----------------------------
-- Records of rooms
-- ----------------------------
BEGIN;
INSERT INTO `rooms` VALUES ('1', 'Meeting Room1', 'red', '30');
COMMIT;

-- ----------------------------
-- Auto increment value for `authorizes`
-- ----------------------------
ALTER TABLE `authorizes` AUTO_INCREMENT=3;

-- ----------------------------
-- Auto increment value for `cars`
-- ----------------------------
ALTER TABLE `cars` AUTO_INCREMENT=1;

-- ----------------------------
-- Auto increment value for `checks`
-- ----------------------------
ALTER TABLE `checks` AUTO_INCREMENT=1;

-- ----------------------------
-- Auto increment value for `drivers`
-- ----------------------------
ALTER TABLE `drivers` AUTO_INCREMENT=2;

-- ----------------------------
-- Auto increment value for `events`
-- ----------------------------
ALTER TABLE `events` AUTO_INCREMENT=5;

-- ----------------------------
-- Auto increment value for `manages`
-- ----------------------------
ALTER TABLE `manages` AUTO_INCREMENT=1;

-- ----------------------------
-- Auto increment value for `reqs`
-- ----------------------------
ALTER TABLE `reqs` AUTO_INCREMENT=4;

-- ----------------------------
-- Auto increment value for `rooms`
-- ----------------------------
ALTER TABLE `rooms` AUTO_INCREMENT=2;
