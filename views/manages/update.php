<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Manages */

$this->title = 'แก้ไขผู้จัดรถ: ' . $model->manage_id;
$this->params['breadcrumbs'][] = ['label' => 'Manages', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->manage_id, 'url' => ['view', 'id' => $model->manage_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="manages-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
