<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Manages */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="manages-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'manage_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'manage_position')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'd_update')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
