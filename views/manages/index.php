<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ManagesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'ผู้จัดรถ';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="manages-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('เพิ่มผู้จัดรถ', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'manage_id',
            'manage_name',
            'manage_position',
            'd_update',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
