<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Manages */

$this->title = 'เพิ่มผู้จัดรถ';
$this->params['breadcrumbs'][] = ['label' => 'ผู้จัดรถ', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="manages-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
