<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Reqs */

$this->title = 'Update Reqs: ' . $model->req_id;
$this->params['breadcrumbs'][] = ['label' => 'Reqs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->req_id, 'url' => ['view', 'id' => $model->req_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="reqs-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_admin', [
        'model' => $model,
    ]) ?>

</div>
