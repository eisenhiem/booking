<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ReqsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'จัดการ การใช้รถ';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="reqs-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('เพิ่มรายการ ขอใช้รถ', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'req_id',
            'req_date',
            'req_by',
            'req_position',
            'req_location',
            //'req_cause',
            //'passengers',
            'begin_datetime',
            'end_datetime',
            //'authorize_id',
            //'authorize_date',
            'car_id',
            'driver_id',
            //'manage_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
