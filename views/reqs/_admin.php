<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use kartik\datetime\DateTimePicker;
use yii\helpers\ArrayHelper;

use app\models\Cars;
use app\models\Drivers;
use app\models\Manages;
use app\models\Authorizes;

$cars = ArrayHelper::map(Cars::find()->all(),'car_id','car_name');
$drivers = ArrayHelper::map(Drivers::find()->all(),'driver_id','driver_name');
$manages = ArrayHelper::map(Manages::find()->all(),'manage_id','manage_name');
$auth = ArrayHelper::map(Authorizes::find()->all(),'authorize_id','authorize_name');

/* @var $this yii\web\View */
/* @var $model app\models\Reqs */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="reqs-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php 
        echo '<label class="control-label">วันที่ขอใช้</label>';
        echo DatePicker::widget([
            'model' => $model,
            'attribute' => 'req_date',
            'value' => date('Y-m-d'),
	        'pluginOptions' => [
		        'autoclose' => true,
	            'format' => 'yyyy-mm-dd'
            ]
        ]); 
    ?>

    <?= $form->field($model, 'req_by')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'req_position')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'req_location')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'req_cause')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'passengers')->textInput() ?>

    <?php 
        echo '<label class="control-label">วัน เวลาเริ่มใช้รถ</label>';
        echo DateTimePicker::widget([
            'model' => $model,
            'attribute' => 'begin_datetime',
            'value' => date('Y-m-d H:i:s'),
	        'pluginOptions' => [
                'autoclose' => true,
		        'format' => 'yyyy-mm-dd hh:ii:ss'
	        ]
        ]); 
        
        echo '<label class="control-label">วัน เวลาสิ้นสุดใช้รถ</label>';
        echo DateTimePicker::widget([
            'model' => $model,
            'attribute' => 'end_datetime',
            'value' => date('Y-m-d H:i:s'),
	        'pluginOptions' => [
	            'autoclose' => true,
	            'format' => 'yyyy-mm-dd hh:ii:ss'
	        ]
        ]); 
    ?>

    <?= $form->field($model, 'authorize_id')->radioList($auth) ?>

    <?php 
        echo '<label class="control-label">วันที่อนุญาต</label>';
        echo DatePicker::widget([
            'model' => $model,
            'attribute' => 'authorize_date',
            'value' => date('Y-m-d'),
	        'pluginOptions' => [
		        'autoclose' => true,
	            'format' => 'yyyy-mm-dd'
            ]
        ]); 
    ?>

    <?= $form->field($model, 'car_id')->radioList($cars) ?>

    <?= $form->field($model, 'driver_id')->radioList($drivers) ?>

    <?= $form->field($model, 'manage_id')->radioList($manages) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
