<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Reqs */

$this->title = $model->req_id;
$this->params['breadcrumbs'][] = ['label' => 'Reqs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="reqs-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->req_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->req_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'req_id',
            'req_date',
            'req_by',
            'req_position',
            'req_location',
            'req_cause',
            'passengers',
            'begin_datetime',
            'end_datetime',
            'authorize_id',
            'authorize_date',
            'car_id',
            'driver_id',
            'manage_id',
        ],
    ]) ?>

</div>
