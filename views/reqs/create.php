<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Reqs */

$this->title = 'การขออนุญาตใช้รถส่วนกลาง';
$this->params['breadcrumbs'][] = ['label' => 'Reqs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="reqs-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
