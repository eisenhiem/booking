<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use kartik\datetime\DateTimePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Reqs */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="reqs-form">
    <?php $form = ActiveForm::begin(); ?>
    <div class="rows">
    <div class="cols-md-12"> 
    <?php 
        echo '<label class="control-label">วันที่ขอใช้</label>';
        echo DatePicker::widget([
            'model' => $model,
            'attribute' => 'req_date',
            'value' => date('Y-m-d'),
	        'pluginOptions' => [
		        'autoclose' => true,
	            'format' => 'yyyy-mm-dd'
            ]
        ]); 
    ?>
    </div>
    </div>
    <div class="rows">
    <div class="cols-md-8"> 
    <?= $form->field($model, 'req_by')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="cols-md-4"> 
    <?= $form->field($model, 'req_position')->textInput(['maxlength' => true]) ?>
    </div>
    </div>
    <div class="rows">
    <div class="cols-md-12"> 
    <?= $form->field($model, 'req_location')->textInput(['maxlength' => true]) ?>
    </div>
    </div>
    <div class="rows">
    <div class="cols-md-8"> 
    <?= $form->field($model, 'req_cause')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="cols-md-4"> 
    <?= $form->field($model, 'passengers')->textInput() ?>
    </div>
    </div>
    <div class="rows">
    <div class="cols-md-6"> 
    <?php 
        echo '<label class="control-label">วัน เวลาเริ่มใช้รถ</label>';
        echo DateTimePicker::widget([
            'model' => $model,
            'attribute' => 'begin_datetime',
            'value' => date('Y-m-d H:i:s'),
	        'pluginOptions' => [
                'autoclose' => true,
		        'format' => 'yyyy-mm-dd hh:ii:ss'
	        ]
        ]); 
    ?>
    </div>
    <div class="cols-md-6"> 
    <?php 
        echo '<label class="control-label">วัน เวลาสิ้นสุดใช้รถ</label>';
        echo DateTimePicker::widget([
            'model' => $model,
            'attribute' => 'end_datetime',
            'value' => date('Y-m-d H:i:s'),
	        'pluginOptions' => [
	            'autoclose' => true,
	            'format' => 'yyyy-mm-dd hh:ii:ss'
	        ]
        ]); 
    ?>
    </div>
    </div>
    <br>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
