<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\ReqsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="reqs-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?php 
        echo '<label class="control-label">วันที่ขอใช้</label>';
        echo DatePicker::widget([
            'model' => $model,
            'attribute' => 'req_date',
            'value' => date('Y-m-d'),
	        'pluginOptions' => [
		        'autoclose' => true,
	            'format' => 'yyyy-mm-dd'
            ]
        ]); 
    ?>

    <?= $form->field($model, 'req_by') ?>

    <?= $form->field($model, 'req_location') ?>

    <?php // echo $form->field($model, 'req_cause') ?>

    <?php // echo $form->field($model, 'passengers') ?>

    <?php // echo $form->field($model, 'begin_datetime') ?>

    <?php // echo $form->field($model, 'end_datetime') ?>

    <?php // echo $form->field($model, 'authorize_id') ?>

    <?php // echo $form->field($model, 'authorize_date') ?>

    <?php // echo $form->field($model, 'car_id') ?>

    <?php // echo $form->field($model, 'driver_id') ?>

    <?php // echo $form->field($model, 'manage_id') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
