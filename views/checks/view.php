<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Checks */

$this->title = $model->check_id;
$this->params['breadcrumbs'][] = ['label' => 'การตรวจเช็ค', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="checks-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->check_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->check_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'check_id',
            'req_id',
            'car_id',
            'chk_date',
            'chk_fuel',
            'fuel_comment',
            'chk_wheel',
            'wheel_comment',
            'chk_outside',
            'outside_comment',
            'chk_mile',
            'mile_comment',
            'chk_status',
            'sender',
            'reciever',
        ],
    ]) ?>

</div>
