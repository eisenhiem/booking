<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Checks */

$this->title = 'แก้ไขการตรวจเช็ค: ' . $model->check_id;
$this->params['breadcrumbs'][] = ['label' => 'Checks', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->check_id, 'url' => ['view', 'id' => $model->check_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="checks-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
