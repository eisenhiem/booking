<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Checks */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="checks-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'req_id')->textInput() ?>

    <?= $form->field($model, 'car_id')->textInput() ?>

    <?= $form->field($model, 'chk_date')->textInput() ?>

    <?= $form->field($model, 'chk_fuel')->dropDownList([ 1 => '1', 0 => '0', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'fuel_comment')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'chk_wheel')->dropDownList([ 1 => '1', 0 => '0', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'wheel_comment')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'chk_outside')->dropDownList([ 1 => '1', 0 => '0', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'outside_comment')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'chk_mile')->dropDownList([ 1 => '1', 0 => '0', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'mile_comment')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'chk_status')->dropDownList([ 2 => '2', 1 => '1', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'sender')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'reciever')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
