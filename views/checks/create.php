<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Checks */

$this->title = 'เพิ่มรายการตรวจสอบ';
$this->params['breadcrumbs'][] = ['label' => 'รายการตรวจสอบ', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="checks-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
