<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ChecksSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'การตรวจเช็ค';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="checks-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('เพิ่มการตรวจเช็ค', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'check_id',
            //'req_id',
            //'car_id',
            'chk_date',
            'chk_fuel',
            //'fuel_comment',
            'chk_wheel',
            //'wheel_comment',
            'chk_outside',
            //'outside_comment',
            'chk_mile',
            //'mile_comment',
            //'chk_status',
            'sender',
            'reciever',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
