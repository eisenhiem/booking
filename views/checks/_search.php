<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ChecksSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="checks-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'check_id') ?>

    <?= $form->field($model, 'req_id') ?>

    <?= $form->field($model, 'car_id') ?>

    <?= $form->field($model, 'chk_date') ?>

    <?= $form->field($model, 'chk_fuel') ?>

    <?php // echo $form->field($model, 'fuel_comment') ?>

    <?php // echo $form->field($model, 'chk_wheel') ?>

    <?php // echo $form->field($model, 'wheel_comment') ?>

    <?php // echo $form->field($model, 'chk_outside') ?>

    <?php // echo $form->field($model, 'outside_comment') ?>

    <?php // echo $form->field($model, 'chk_mile') ?>

    <?php // echo $form->field($model, 'mile_comment') ?>

    <?php // echo $form->field($model, 'chk_status') ?>

    <?php // echo $form->field($model, 'sender') ?>

    <?php // echo $form->field($model, 'reciever') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
