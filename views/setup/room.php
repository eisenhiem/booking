<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\RoomsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Rooms';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rooms-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('เพิ่มห้องประชุม', ['setup/createroom'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'room_id',
            'room_name',
            'room_color',
            'room_capacity',
            [
                'class' => 'yii\grid\ActionColumn',
                'options'=>['style'=>'width:120px;'],
                'buttonOptions'=>['class'=>'btn btn-primary'],
                'template'=>'{updateroom}',
                'buttons'=>[
                    'updateroom' => function($url,$model,$key){
                      return Html::a('Update',['setup/updateroom','id'=>$model->room_id],['class' => 'btn btn-success']);
                    }
                ]
             ],
             [
                'class' => 'yii\grid\ActionColumn',
                'options'=>['style'=>'width:120px;'],
                'buttonOptions'=>['class'=>'btn btn-primary'],
                'template'=>'{deleteroom}',
                'buttons'=>[
                    'deleteroom' => function($url,$model,$key){
                      return Html::a('Delete',['setup/deleteroom','id'=>$model->room_id],['class' => 'btn btn-success']);
                    }
                ]
             ],
        ],
    ]); ?>


</div>
