<?php
use app\models\Rooms;
use app\models\Events;

$rooms = Rooms::find()->select('room_id as id,room_name as title')->asArray()->all();
$events = Events::find()->select('event_id as id,room_id as resourceId,start_date as start,end_date as end,event_name as title,eventColor as color')->asArray()->all();
/* @var $this yii\web\View */

$this->title = 'Booking app';
?>
<div class="site-index">
<?= \edofre\fullcalendarscheduler\FullcalendarScheduler::widget([
	'header'        => [
//		'left'   => 'today prev,next',
		'right'   => 'next',
        'center' => 'title',
//		'right'  => 'timelineDay,timelineThreeDays,agendaWeek,month',
		'left'  => 'prev timelineDay,agendaWeek,month',
],
	'clientOptions' => [
        'schedulerLicenseKey' => 'GPL-My-Project-Is-Open-Source',
		'now'               => date('Y-m-d'),
		'editable'          => false, // enable draggable events
		'aspectRatio'       => 1.8,
        'scrollTime'        => '08:00', // undo default 6am scrollTime
        'timeFormat'        => 'HH:mm',
		'defaultView'       => 'month',
		'views'             => [
			'timelineThreeDays' => [
				'type'     => 'timeline',
				'duration' => [
					'days' => 3,
				],
			],
		],
		'resourceLabelText' => 'Rooms',
		'resources'         => $rooms,
        'events'            => $events,
	],
]);
?>
</div>
