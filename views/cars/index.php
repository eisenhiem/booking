<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CarsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'ยานพาหนะ';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cars-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('เพิ่มยาานพาหนะ', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'car_id',
            'car_name',
            'car_band',
            'car_no',
            'car_year',
            //'car_maintenance',
            //'car_owner',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
