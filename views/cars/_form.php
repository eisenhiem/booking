<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Cars */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cars-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'car_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'car_band')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'car_no')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'car_year')->textInput() ?>

    <?= $form->field($model, 'car_maintenance')->textInput() ?>

    <?= $form->field($model, 'car_check_date')->textInput() ?>

    <?= $form->field($model, 'car_owner')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
