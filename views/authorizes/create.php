<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Authorizes */

$this->title = 'เพิ่มผู้มีสิทธิอนุมัติ';
$this->params['breadcrumbs'][] = ['label' => 'Authorizes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="authorizes-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
