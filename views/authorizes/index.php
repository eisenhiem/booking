<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AuthorizesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'ผู้มีสิทธิอนุมัติ';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="authorizes-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('เพิ่มผู้มีสิทธิอนุมัติ', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'authorize_id',
            'authorize_name',
            'authorize_position_1',
            'authorize_position_2',
            'authorize_position_3',
            //'d_update',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
