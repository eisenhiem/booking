<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Authorizes */

$this->title = 'แก้ไขผู้มีสิทธิอนุมัติ: ' . $model->authorize_id;
$this->params['breadcrumbs'][] = ['label' => 'Authorizes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->authorize_id, 'url' => ['view', 'id' => $model->authorize_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="authorizes-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
