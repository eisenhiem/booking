<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AuthorizesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="authorizes-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'authorize_id') ?>

    <?= $form->field($model, 'authorize_name') ?>

    <?= $form->field($model, 'authorize_position_1') ?>

    <?= $form->field($model, 'authorize_position_2') ?>

    <?= $form->field($model, 'authorize_position_3') ?>

    <?php // echo $form->field($model, 'd_update') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
