<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Authorizes */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="authorizes-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'authorize_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'authorize_position_1')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'authorize_position_2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'authorize_position_3')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
