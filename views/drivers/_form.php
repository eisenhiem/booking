<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Drivers */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="drivers-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'driver_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'driver_license')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'license_expire')->textInput() ?>
    <?php 
        echo '<label class="control-label">วันที่อนุญาต</label>';
        echo DatePicker::widget([
            'model' => $model,
            'attribute' => 'license_expire',
            'value' => date('Y-m-d'),
	        'pluginOptions' => [
		        'autoclose' => true,
	            'format' => 'yyyy-mm-dd'
            ]
        ]); 
    ?>

    <?= $form->field($model, 'driver_contact')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
