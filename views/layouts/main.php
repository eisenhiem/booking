<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
// Bootstrap4 
// use yii\bootstrap4\Nav;
// use yii\bootstrap4\NavBar;
// use yii\bootstrap4\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            ['label' => 'Home', 'url' => ['/site/index']],
            ['label' => 'รายการจองห้องประชุม', 'url' => ['/events/index']],
            ['label' => 'ตรวจสอบพื้นที่เสี่ยง', 'url' => ['/area/index']],
            ['label' => 'รายการขอใช้รถ', 'url' => ['/reqs/index']],
            ['label' => 'จัดการการใช้รถ', 'visible' => !Yii::$app->user->isGuest, 'url' => ['/reqs/admin']],
            ['label' => 'จัดการส่งมอบรถ', 'visible' => !Yii::$app->user->isGuest, 'url' => ['/checks/index']],
            ['label' => 'ตั้งค่า', 'visible' => !Yii::$app->user->isGuest, 'items' => [
                ['label' => 'ตั้งค่าผู้มีอำนาจอนุมัติ' ,'url' => ['/authorizes/index']],
                ['label' => 'ตั้งค่าพนักงานขับรถ' ,'url' => ['/drivers/index']],
                ['label' => 'ตั้งค่ายานพาหนะ' ,'url' => ['/cars/index']],
                ['label' => 'ตั้งค่าผู้จัดรถ' ,'url' => ['/manages/index']],
                ['label' => 'ตั้งค่าห้องประชุม' ,'url' => ['/setup/room']],
            ] ],
            Yii::$app->user->isGuest ? (
                ['label' => 'Login', 'url' => ['/site/login']]
            ) : (
                '<li>'
                . Html::beginForm(['/site/logout'], 'post')
                . Html::submitButton(
                    'Logout (' . Yii::$app->user->identity->username . ')',
                    ['class' => 'btn btn-link logout']
                )
                . Html::endForm()
                . '</li>'
            )
        ],
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; My Company <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
