<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\datetime\DateTimePicker;
use yii\helpers\ArrayHelper;

use app\models\Rooms;

$rooms = ArrayHelper::map(Rooms::find()->all(),'room_id','room_name');
$color = [
    'Red'  => '<div class="panel" style="background:red;color:white;width:50px"> <br> </div>',
    'Blue' => '<div class="panel" style="background:blue;color:white;width:50px"> <br> </div>' ,
    'Green' => '<div class="panel" style="background:green;color:white;width:50px"> <br> </div>' , 
    'Gold' => '<div class="panel" style="background:gold;color:white;width:50px"> <br> </div>', 
    'Black' => '<div class="panel" style="background:black;color:white;width:50px"> <br> </div>', 
    'Orange' => '<div class="panel" style="background:orange;color:white;width:50px"> <br> </div>', 
    'Purple' => '<div class="panel" style="background:purple;color:white;width:50px"> <br> </div>', 
    'Magenta' => '<div class="panel" style="background:magenta;color:white;width:50px"> <br> </div>' , 
    'Brown' => '<div class="panel" style="background:brown;color:white;width:50px"> <br> </div>' ];
$color = [
    'Red' => 'สีแดง',
    'Blue' => 'สีน้ำเงิน',
    'Green' => 'สีเขียว',
    'Gold' => 'สีเหลือง',
    'Orange' => 'สีส้ม',
    'Brown' => 'สีน้ำตาล',
    'Purple' => 'สีม่วง',
    'Magenta' => 'สีชมพู',
    'Black' => 'สีดำ',
];
    /* @var $this yii\web\View */
/* @var $model app\models\Events */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="events-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <?= $form->field($model, 'room_id')->radioList($rooms) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <?= $form->field($model, 'event_name')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4 col-sm-12">
            <?php 
                echo '<label class="control-label">วัน เวลาเริ่มประชุม</label>';
                echo DateTimePicker::widget([
                    'model' => $model,
                    'attribute' => 'start_date',
	                'value' => date('Y-m-d H:i:s'),
	                'pluginOptions' => [
		                'autoclose' => true,
		                'format' => 'yyyy-mm-dd hh:ii:ss'
	                ]
                ]); 
            ?>
        </div>
        <div class="col-md-4 col-sm-12">
            <?php 
                echo '<label class="control-label">วัน เวลาสิ้นสุดประชุม</label>';
                echo DateTimePicker::widget([
                    'model' => $model,
                    'attribute' => 'end_date',
	                'value' => date('Y-m-d H:i:s'),
	                'pluginOptions' => [
		                'autoclose' => true,
		                'format' => 'yyyy-mm-dd hh:ii:ss'
	                ]
                ]); 
            ?>
        </div>
        <div class="col-md-4 col-sm-12">
            <?= $form->field($model, 'participants')->textInput() ?>
        </div>
    </div>
    <?= $form->field($model, 'snacks')->checkbox() ?>
    <?= $form->field($model, 'lunch')->checkbox() ?>
    <?= $form->field($model, 'projector')->checkbox() ?>
    <?= $form->field($model, 'conference')->checkbox() ?>
    <?= $form->field($model, 'recorder')->checkbox() ?>
    <?= $form->field($model, 'request_by')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'eventColor')->radioList($color) ?>

    <?php /*= $form->field($model, 'eventColor')->radioList($color,['item' => function($index, $label, $name, $value) {
        $return = '<label class="modal-radio">';
        $return .= '<input type="radio" name="' . $name . '" value="' . $value . '" tabindex="1">';
        //$return .= '<i></i>';
        $return .=  ucwords($label) ;
        $return .= '</label>';
        return $return;}])->label(false); */?>
    <div class="form-group">
        <?= Html::submitButton('บันทึก', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
