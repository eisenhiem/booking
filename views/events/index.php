<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\icons\Icon;
Yii::$app->formatter->locale = 'th_TH';

/* @var $this yii\web\View */
/* @var $searchModel app\models\EventsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Events';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="events-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('เพิ่มการประชุม', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'event_id',
            //'rooms.room_name',
            'event_name',
            'start_date',
            'end_date',
            'participants',
            //'snacks',
            //'projector',
            //'conference',
            //'recorder',
            // แสดงข้อมูลออกเป็น icon
            [ 
                'attribute' => 'snacks',
                'format'=>'html',
                'value'=>function($model, $key, $index, $column){
                  return $model->snacks==1 ? "<span style=\"color:green;\"><i class=\"glyphicon glyphicon-ok\"></i></span>" : "<span style=\"color:red;\"><i class=\"glyphicon glyphicon-remove\"></i></span>";
                }
            ],
            [ 
                'attribute' => 'lunch',
                'format'=>'html',
                'value'=>function($model, $key, $index, $column){
                  return $model->lunch==1 ? "<span style=\"color:green;\"><i class=\"glyphicon glyphicon-ok\"></i></span>" : "<span style=\"color:red;\"><i class=\"glyphicon glyphicon-remove\"></i></span>";
                }
            ],
            [ 
                'attribute' => 'projector',
                'format'=>'html',
                'value'=>function($model, $key, $index, $column){
                  return $model->projector==1 ? "<span style=\"color:green;\"><i class=\"glyphicon glyphicon-ok\"></i></span>" : "<span style=\"color:red;\"><i class=\"glyphicon glyphicon-remove\"></i></span>";
                }
            ],
            [ 
                'attribute' => 'conference',
                'format'=>'html',
                'value'=>function($model, $key, $index, $column){
                  return $model->conference==1 ? "<span style=\"color:green;\"><i class=\"glyphicon glyphicon-ok\"></i></span>" : "<span style=\"color:red;\"><i class=\"glyphicon glyphicon-remove\"></i></span>";
                }
            ],
            [ 
                'attribute' => 'recorder',
                'format'=>'html',
                'value'=>function($model, $key, $index, $column){
                  return $model->recorder==1 ? "<span style=\"color:green;\"><i class=\"glyphicon glyphicon-ok\"></i></span>" : "<span style=\"color:red;\"><i class=\"glyphicon glyphicon-remove\"></i></span>";
                }
            ],
            'request_by',
            //'request_date',
            [
                'class' => 'yii\grid\ActionColumn',
                'options'=>['style'=>'width:120px;'],
                'buttonOptions'=>['class'=>'btn btn-primary'],
                'template'=>'{view}',
                'buttons'=>[
                    'view' => function($url,$model,$key){
                      return Html::a('รายละเอียด',['events/view','id'=>$model->event_id],['class' => 'btn btn-info']);
                    }
                ]
             ],
            [
                'class' => 'yii\grid\ActionColumn',
                'options'=>['style'=>'width:120px;'],
                'buttonOptions'=>['class'=>'btn btn-primary'],
                'template'=>'{update}',
                'buttons'=>[
                    'update' => function($url,$model,$key){
                      return Html::a('แก้ไข',['events/update','id'=>$model->event_id],['class' => 'btn btn-success']);
                    }
                ]
             ],
             [
                'class' => 'yii\grid\ActionColumn',
                'options'=>['style'=>'width:120px;'],
                'buttonOptions'=>['class'=>'btn btn-primary'],
                'template'=>'{delete}',
                'buttons'=>[
                    'delete' => function($url,$model,$key){
                      return Html::a('ลบ',['events/delete','id'=>$model->event_id],['class' => 'btn btn-danger']);
                    }
                ]
             ],
        ],
    ]); ?>


</div>
